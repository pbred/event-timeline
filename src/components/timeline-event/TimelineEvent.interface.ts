export default interface ITimelineEvent {
    era: 'bce' | 'ce';
    startYear: number | undefined;
    endYear: number | undefined;
    isStartApproximate: boolean;
    isEndApproximate: boolean;
}
