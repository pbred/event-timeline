import TimelineEvent from '@/components/timeline-event/TimelineEvent.vue';
import { mount } from '@vue/test-utils';

describe('TimelineEvent.vue', () => {
  it('should render', () => {

    const wrapper = mount(TimelineEvent),
          selector = '.timeline-event',
          el = wrapper.find(selector);

    expect(el.is(selector)).toBe(true);
  });
});
