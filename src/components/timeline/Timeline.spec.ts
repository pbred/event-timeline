import Timeline from '@/components/timeline/Timeline.vue';
import { mount } from '@vue/test-utils';

describe('Timeline.vue', () => {
  it('should render', () => {

    const wrapper = mount(Timeline),
    selector = '.timeline',
    el = wrapper.find(selector);

    expect(el.is(selector)).toBe(true);
  });
});
