import ITimelineEvent from '../timeline-event/TimelineEvent.interface';

export default interface ITimeline {
    timelineName: string | undefined;
    timelineWeight: number | undefined;
    events: ITimelineEvent[];
}
