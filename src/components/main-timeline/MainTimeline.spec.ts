import MainTimeline from '@/components/main-timeline/MainTimeline.vue';
import { mount } from '@vue/test-utils';

describe('MainTimeline.vue', () => {
  it('should render', () => {

    const wrapper = mount(MainTimeline),
          selector = '.main-timeline',
          el = wrapper.find(selector);

    expect(el.is(selector)).toBe(true);
  });
});
