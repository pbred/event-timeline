import ITimeline from '../timeline/Timeline.interface';

export default interface IMainTimeline {
    timelines: ITimeline[];
}
