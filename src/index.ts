import Vue from 'vue';
import MainTimeline from './components/main-timeline/MainTimeline.vue';

export const app = new Vue({
    el: '#app',
    template: `
        <MainTimeline></MainTimeline>
    `,
    components: {
        MainTimeline,
    },
});
