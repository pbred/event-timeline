# Event Timeline

A web application to display historical Earth history timelines using the [Common Era calendar notation system](https://en.wikipedia.org/wiki/Common_Era).

It was built with Vue.js 2.x along with TypeScript.

## Development

Install dependencies:

`npm install`

Build the project:

`npm run build`

Run the project in development mode:

`npm run dev`

Run the tests:

`npm run test`

## Credits

This project was based on Daniel Rossenwasser's [typescript-vue-tutorial](https://github.com/DanielRosenwasser/typescript-vue-tutorial).